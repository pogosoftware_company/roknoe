# How to run `Roknoe` application

## Requirements

* [Node](https://nodejs.org/en/download/)

* [npm](https://www.npmjs.com/get-npm)

* Angular CLI

    ```bash
    npm install -g @angular/cli
    ```

* [.NET Core](https://www.microsoft.com/net/download/)

* [Docker](https://www.docker.com/community-edition#/download)

## Build and run

### Website (frontend)

In folder `Roknoe.Website` execute

```bash
npm install
```

```bash
ng serve
```

### API (backend)

In folder `Roknoe.Api` execute

```bash
dotnet run
```

### EmailService (worker)

In folder `Roknoe.EmailService` execute

```bash
dotnet run
```

## Containers

### MongoDB

```bash
docker container run -d -p 27017:27017 mongo
```

### RabbitMQ

```bash
docker container run -d -p 15672:15672 -p 5672:5672 rabbitmq:3-management
```

### MailHog

```bash
docker container run -d -p 8025:8025 -p 1025:1025 mailhog/mailhog
```
