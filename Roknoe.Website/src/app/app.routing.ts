import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EventsComponent } from '@event/components/events/events.component';
import { NewEventComponent } from '@event/components/newEvent/new-event.component';

const appRoutes: Routes= [
    { 
        path: '', 
        redirectTo: 'events',
        pathMatch: 'full' 
    },
    {
        path: 'events',
        component: EventsComponent
    },
    {
        path: 'add-new-event',
        component: NewEventComponent
    },
    { 
        path: '**', 
        redirectTo: 'events' 
    }
];

export const routing : ModuleWithProviders = RouterModule.forRoot(appRoutes);