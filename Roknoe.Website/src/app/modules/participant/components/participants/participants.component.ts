import { Component, OnInit, Input } from '@angular/core';
import { Participant } from '@participant/models/participant.interface';
import { ParticipantService } from '@participant/participant.service';

@Component({
    selector: 'participants',
    templateUrl: './participants.component.html'
})
export class ParticipantsComponent implements OnInit {

    @Input() eventId: string;
    participants: Participant[];

    constructor(private participantService: ParticipantService) { }

    ngOnInit() {
        this.getParticipants();
    }

    getParticipants() {
        this.participantService.getEventParticipants(this.eventId)
            .subscribe(
                data => this.participants = data,
                error => console.log(error));
    }

    onQuitEvent(name: string) {
        this.participantService.quitEvent(this.eventId, name)
            .subscribe(
                data => this.getParticipants()
            );
    }
}
