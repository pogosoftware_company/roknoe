import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ParticipantService } from '@participant/participant.service';
import { SignUpForEvent } from '@participant/models/signUpForEvent.interface';

@Component({
    selector: 'sign-up-for-event',
    templateUrl: './sign-up-for-event.component.html'
})
export class SignUpForEventComponent implements OnInit {
    @Input() eventId: string;
    @Input() eventName: string;
    @Output() onSignupDone = new EventEmitter<boolean>();

    form: FormGroup;
    private modalRef: NgbModalRef;

    constructor(private modalService: NgbModal,
        private participantService: ParticipantService,
        private formBuilder: FormBuilder) { }

    ngOnInit() {
        this.form = this.formBuilder.group({
            name: ['', Validators.required],
            email: ['', Validators.email]
        });
    }

    open(content) {
        this.modalRef = this.modalService.open(content);
    }

    onSubmit() {
        let participant = <SignUpForEvent>{};
        participant.name = this.form.value.name;
        participant.email = this.form.value.email;

        this.participantService.signUpForEvent(this.eventId, participant).subscribe(res => {
            this.form.reset();
            this.modalRef.close();
            this.onSignupDone.emit(true);
        });
    }

    onCancel() {
        this.form.reset();
        this.modalRef.close();
        this.onSignupDone.emit(false);
    }
}
