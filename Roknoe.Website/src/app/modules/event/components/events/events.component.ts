import { Component, OnInit } from '@angular/core';
import { Event } from '@event/models/event.interface';
import { EventService } from '@event/event.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
    selector: 'events',
    templateUrl: './events.component.html',
    styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
    events: Event[];
    eventName = "";
    eventId = "";
    private EventsList = new BehaviorSubject<Array<Event>>(this.events);

    constructor(private eventService: EventService) { }

    ngOnInit() {
        this.getAllEvents();
    }

    getAllEvents() {
        this.eventService.getAllEvents()
            .subscribe(
                data => this.events = data,
                error => console.log(error));
    }

    onSignUpClose($event) {
        if ($event) {
            this.getAllEvents();
        }
    }
}
