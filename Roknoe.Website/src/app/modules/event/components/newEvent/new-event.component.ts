import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { NewEvent } from '@event/models/newEvent.interface';
import { EventService } from '@event/event.service';

@Component({
    selector: 'new-event',
    templateUrl: './new-event.component.html'
})
export class NewEventComponent implements OnInit {
    newEventForm: FormGroup;

    constructor(private eventService: EventService,
        private router: Router,
        private formBuilder: FormBuilder) { }

    ngOnInit() {
        this.newEventForm = this.formBuilder.group({
            eventName: ['', Validators.required],
            eventDate: ['', Validators.compose([ Validators.required ])],
            place: ['', Validators.required],
            description: ['', Validators.required]
        });
    }

    onSubmit() {
        var newEvent = <NewEvent>{};
        newEvent.eventName = this.newEventForm.value.eventName;
        newEvent.eventDate = this.newEventForm.value.eventDate;
        newEvent.place = this.newEventForm.value.place;
        newEvent.description = this.newEventForm.value.description;

        this.eventService.addNewEvent(newEvent).subscribe(res => {
            this.newEventForm.reset();
            this.router.navigate(['events']);
        });
    }

    onBack() {
        this.newEventForm.reset();
        this.router.navigate(['events']);
    }

    getFormControlName(name: string) {
        return this.newEventForm.get(name);
    }

    hasErrors(name: string): boolean {
        var e = this.getFormControlName(name);
        return e && (e.dirty || e.touched) && !e.valid;
    }
}
