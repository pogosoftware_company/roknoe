export interface NewEvent {
    eventName : string;
    eventDate : Date;
    place : string;
    description: string;
}