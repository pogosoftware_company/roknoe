import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Components
import { EventsComponent } from '@event/components/events/events.component';
import { NewEventComponent } from '@event/components/newEvent/new-event.component';

// Services
import { EventService } from './event.service';

// Modules
import { ParticipantModule } from '@participant/participant.module';

@NgModule({
    declarations: [ EventsComponent, NewEventComponent ],
    imports: [ CommonModule, HttpModule, FormsModule, ReactiveFormsModule, ParticipantModule ],
    exports: [],
    providers: [ EventService ]
})
export class EventModule {}