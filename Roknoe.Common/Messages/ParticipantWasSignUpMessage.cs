using System;

namespace Roknoe.Common.Messages
{
    public class ParticipantWasSignUpMessage
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string EventName { get; set; }

        public DateTime EventDate { get; set; }

        public string Place { get; set; }
    }
}
