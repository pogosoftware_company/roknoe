using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Roknoe.Api.Models.Inbound
{
    public class AddEvent
    {
        [Required]
        public string EventName { get; set; }

        [Required]
        public DateTime EventDate { get; set; }

        [Required]
        public DateTime EntriesTo { get; set; }

        [Required]
        public string Place { get; set; }

        public string Description { get; set; }

        public ICollection<Agenda> Agenda { get; set; }
    }
}
