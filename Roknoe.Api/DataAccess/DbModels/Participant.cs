namespace Roknoe.Api.DataAccess.DbModels
{
    public class Participant
    {
        public string Name { get; set; }

        public string Email { get; set; }
    }
}