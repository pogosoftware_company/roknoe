using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Roknoe.Api.DataAccess.DbModels;

namespace Roknoe.Api.DataAccess.Repositories
{
    public class EventRepository : IEventRepository
    {
        private readonly IMongoDbContext _context;

        public EventRepository(IMongoDbContext context)
        {
            _context = context;
        }

        public async Task<ICollection<Event>> GetAllEventsAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _context.Events
                .AsQueryable()
                .OrderByDescending(workshop => workshop.CreatedAt)
                .ToListAsync(cancellationToken);
        }

        public async Task<Event> GetEventAsync(Guid eventId, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _context.Events
                .AsQueryable()
                .Where(x => x.Id.Equals(eventId))
                .SingleOrDefaultAsync(cancellationToken);
        }

        public async Task AddNewEventAsync(Event eventToAdd, CancellationToken cancellationToken = default(CancellationToken))
        {
            await _context.Events.InsertOneAsync(eventToAdd, cancellationToken: cancellationToken);
        }
    }
}
