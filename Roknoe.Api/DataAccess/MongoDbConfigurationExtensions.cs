using System;
using System.Linq;
using Microsoft.Extensions.Configuration;

namespace Roknoe.Api.DataAccess
{
    public static class MongoDbConfigurationExtensions
    {
        private static string mongoDbSectionName = "MongoDb";
       
        public static IConfigurationSection GetMongoDbConfigurationSection(this IConfiguration configuration)
        {
            var section = configuration.GetSection(mongoDbSectionName);
            EnsureSpecificSectionExists(section, mongoDbSectionName);
            
            return section;
        }

        private static void EnsureSpecificSectionExists(IConfigurationSection section, string sectionName)
        {
            if (!section.GetChildren().Any())
			{
				throw new ArgumentException($"Unable to configuration section '{sectionName}'. Make sure it exists in the provided configuration");
			}
        }
    }
}