using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using RawRabbit;
using Roknoe.Api.DataAccess.Repositories;
using Roknoe.Api.Extensions;
using Roknoe.Api.Models.Inbound;
using Roknoe.Api.Models.Outbound;
using Roknoe.Common.Messages;
using Roknoe.Common.RabbitMq;
using DbParticipant = Roknoe.Api.DataAccess.DbModels.Participant;

namespace Roknoe.Api.Services
{
    public class ParticipantService : IParticipantService
    {
        private readonly IMapper _mapper;
        private readonly IParticipantRepository _participantRepository;
        private readonly IBusClient _busClient;

        public ParticipantService(IMapper mapper,
            IParticipantRepository participantRepository,
            IBusClient busClient)
        {
            _mapper = mapper;
            _participantRepository = participantRepository;
            _busClient = busClient;
        }

        public async Task SignUpForEventAsync(Guid eventId, SignUpForEvent request, CancellationToken cancellationToken = default(CancellationToken))
        {
            var participant = _mapper.Map<DbParticipant>(request);
            var eventDetails = await _participantRepository.SignUpForEventAsync(eventId, participant, cancellationToken);

            var message = _mapper.MapInto<ParticipantWasSignUpMessage>(participant, eventDetails);
            await _busClient.PublishAsync(message, token: cancellationToken);
        }

        public async Task<ICollection<Participant>> GetEventParticipantsAsync(Guid eventId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var participants = await _participantRepository.GetEventParticipantsAsync(eventId, cancellationToken);
            return _mapper.Map<ICollection<Participant>>(participants);
        }

        public Task QuitEventAsync(Guid eventId, string name, CancellationToken cancellationToken = default(CancellationToken))
        {
            return _participantRepository.QuitEventAsync(eventId, name, cancellationToken);
        }
    }
}
