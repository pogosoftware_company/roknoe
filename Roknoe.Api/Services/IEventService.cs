using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Roknoe.Api.Models.Inbound;
using Roknoe.Api.Models.Outbound;

namespace Roknoe.Api.Services
{
    public interface IEventService
    {
        Task<ICollection<Event>> GetAllEventsAsync(CancellationToken cancellationToken = default(CancellationToken));

        Task<Event> GetEventAsync(Guid eventId, CancellationToken cancellationToken = default(CancellationToken));

        Task<Event> AddNewEventAsync(AddEvent request, CancellationToken cancellationToken = default(CancellationToken));
    }
}
