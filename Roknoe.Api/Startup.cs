﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RawRabbit.Configuration;
using RawRabbit.vNext;
using RawRabbit.vNext.Pipe;
using Roknoe.Api.DataAccess;
using Roknoe.Api.DataAccess.Repositories;
using Roknoe.Api.Filters;
using Roknoe.Api.MessageFormatters;
using Roknoe.Api.Models.Inbound;
using Roknoe.Api.Services;
using Roknoe.Common.Messages;
using Roknoe.Common.RabbitMq;
using Swashbuckle.AspNetCore.Swagger;

namespace Roknoe.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            RegisterDb(services);
            RegisterRabbitMq(services);
            RegisterServices(services);

            services.AddAutoMapper();
            services.AddCors();
            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(ValidateModelStateFilter));
            });
            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new Info { Title = "Event API", Version="v1"});
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder =>
                builder.WithOrigins("http://localhost:4200")
                    .AllowAnyHeader()
                    .AllowAnyMethod()
            );

            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Event API V1");
            });

            var redirectRootToSwagger = new RewriteOptions()
                .AddRedirect("$", "swagger");
            app.UseRewriter(redirectRootToSwagger);
        }

        private void RegisterDb(IServiceCollection services)
        {
            services.Configure<MongoDbConfiguration>(Configuration.GetMongoDbConfigurationSection());
            services.AddSingleton<IMongoDbContext, MongoDbContext>();
            services.AddTransient<IEventRepository, EventRepository>();
            services.AddTransient<IParticipantRepository, ParticipantRepository>();
        }

        private void RegisterServices(IServiceCollection services)
        {
            services.AddTransient<IEventService, EventService>();
            services.AddTransient<IParticipantService, ParticipantService>();
        }

        private void RegisterRabbitMq(IServiceCollection services)
        {
            services.AddRawRabbit(new RawRabbitOptions {
                ClientConfiguration = Configuration
                    .GetRabbitMqConfigurationSection()
                    .Get<RawRabbitConfiguration>()
            });
            services.AddTransient<IMessageFormatter<SignUpForEvent, ParticipantWasSignUpMessage>, ParticipantWasSignUpMessageFormatter>();
            services.AddTransient(typeof(IPublisher<,>), typeof(Publisher<,>));
        }
    }
}
