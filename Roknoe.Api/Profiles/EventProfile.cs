using System;
using System.Collections.ObjectModel;
using AutoMapper;
using Roknoe.Api.DataAccess.DbModels;
using Roknoe.Api.Models.Inbound;

namespace Roknoe.Api.Profiles
{
    public class EventProfile : Profile
    {
        public EventProfile()
        {
            CreateMap<DataAccess.DbModels.Event, Models.Outbound.Event>()
                .ForMember(dest => dest.EventId, opt => opt.MapFrom(src => src.Id));
            CreateMap<AddEvent, DataAccess.DbModels.Event>()
                .ForMember(dest => dest.CreatedAt, opt => opt.UseValue(DateTime.UtcNow))
                .ForMember(dest => dest.Participants, opt => opt.UseValue(new Collection<Participant>()));
        }
    }
}
