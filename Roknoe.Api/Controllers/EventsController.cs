using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Roknoe.Api.Models.Inbound;
using Roknoe.Api.Services;

namespace Roknoe.Api.Controllers
{
    [Route("/api/events")]
    [Produces("application/json")]
    public class EventsController : Controller
    {
        private readonly IEventService _eventService;

        public EventsController(IEventService eventService)
        {
            _eventService = eventService;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllEventsAsync() =>
            Ok(await _eventService.GetAllEventsAsync());

        [HttpGet("{eventId}", Name="GetEventAsync")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetEventAsync(Guid eventId) =>
            Ok(await _eventService.GetEventAsync(eventId));

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<IActionResult> AddNewEventAsync([FromBody]AddEvent request)
        {
            var newEvent = await _eventService.AddNewEventAsync(request);
            return CreatedAtRoute("GetEventAsync", new { eventId = newEvent.EventId}, newEvent);
        }
    }
}
