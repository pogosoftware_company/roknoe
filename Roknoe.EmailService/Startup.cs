﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RawRabbit;
using RawRabbit.Configuration;
using RawRabbit.vNext;
using RawRabbit.vNext.Pipe;
using Roknoe.Common.Messages;
using Roknoe.Common.RabbitMq;
using Roknoe.EmailService.Configuration;
using Roknoe.EmailService.EmailClients;
using Roknoe.EmailService.Extensions;
using Roknoe.EmailService.MessageHandlers;

namespace Roknoe.EmailService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRawRabbit(new RawRabbitOptions {
                ClientConfiguration = Configuration
                    .GetRabbitMqConfigurationSection()
                    .Get<RawRabbitConfiguration>()
            });

            services.Configure<EmailConfiguration>(
                Configuration.GetEmailConfigurationSection());

            services.AddTransient<IEmailClient, EmailClient>();
            services.AddTransient<IMessageHandler<ParticipantWasSignUpMessage>, ParticipantWasSignUpMessageHandler>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
            IBusClient busClient)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var participantWasSignUpHandler = app
                .ApplicationServices
                .GetService<IMessageHandler<ParticipantWasSignUpMessage>>();

            busClient.SubscribeAsync<ParticipantWasSignUpMessage>(async message => {
                await participantWasSignUpHandler.HandleAsync(message);
            });
        }
    }
}
