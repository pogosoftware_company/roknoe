using System.Threading.Tasks;

namespace Roknoe.EmailService.MessageHandlers
{
    public interface IMessageHandler<TMessage>
    {
        Task HandleAsync(TMessage message);
    }
}